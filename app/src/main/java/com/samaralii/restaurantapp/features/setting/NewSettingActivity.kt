package com.samaralii.restaurantapp.features.setting

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import com.samaralii.restaurantapp.*
import kotlinx.android.synthetic.main.activity_newsetting.*
import kotlinx.android.synthetic.main.layout_main_background.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import yuku.ambilwarna.AmbilWarnaDialog
import java.io.File
import java.io.FileOutputStream


class NewSettingActivity: BaseActivity(), SettingDialog.SettingDialogListener, CurrencyDialog.CurrencyDialogListener {

    var colorIntBg: Int? = null
    var colorIntDetailText: Int? = null
    var colorIntTitleText: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_newsetting)
        init()
    }

    private fun init() {


        val bgColor = sp.getInt(AppConst.COLOR_INT_BG, Color.WHITE)
        setting_colorBg.setBackgroundColor(bgColor)

        val detailTextColor = sp.getInt(AppConst.COLOR_INT_DETAIL_TEXT, Color.WHITE)
        setting_colorText.setBackgroundColor(detailTextColor)

        val menuTitleColor = sp.getInt(AppConst.COLOR_INT_TITLE_TEXT, Color.BLACK)
        setting_colorTextTitle.setBackgroundColor(menuTitleColor)



        val btnBg = sp.getInt(AppConst.COLOR_START_BTN, Color.BLACK)
        setting_startBtnColor.setBackgroundColor(btnBg)


        val btnTxtColor = sp.getInt(AppConst.COLOR_START_BTN_TXT, Color.WHITE)
        setting_startBtnTxtColor.setBackgroundColor(btnTxtColor)


        val mainBg = sp.getInt(AppConst.COLOR_MAIN_BG, Color.WHITE)
        setting_mainBgColor.setBackgroundColor(mainBg)


        setting_apiurl.text = sp.getString(AppConst.BASE_URL_KEY, "")


        if (sp.getString(AppConst.LOGO_URL_KEY, null) != null) {

            try {
                GlideApp.with(this).load(sp.getString(AppConst.LOGO_URL_KEY, "")).into(setting_logo_image)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }


        }

        if (sp.getInt(AppConst.GRID_VIEW, 0) == AppConst.TRUE) {

            setting_swGrid.isChecked = true
            showGridViews(true)

        } else if (sp.getInt(AppConst.GRID_VIEW, 0) == AppConst.FALSE){

            setting_swGrid.isChecked = false
            showGridViews(false)

        }


        if (sp.getBoolean(AppConst.IS_MAIN_IMAGE, false)) {
            setting_rbSetBgColor.isChecked = false
            setting_rbSetBgImage.isChecked = true
            showHideMainBgColor(false)
        } else {
            setting_rbSetBgColor.isChecked = true
            setting_rbSetBgImage.isChecked = false
            showHideMainBgColor(true)
        }


        val showCal = sp.getBoolean(AppConst.SHOW_CAL, true)
        setting_cbCal.isChecked = showCal

        val showTime = sp.getBoolean(AppConst.SHOW_TIME, true)
        setting_cbTime.isChecked = showTime

        setting_subMenu.isChecked = sp.getInt(AppConst.SUB_MENU, 0) == AppConst.TRUE

        setting_swGrid.setOnCheckedChangeListener { _, isChecked ->


            if (isChecked) {
                showGridViews(true)
                repository.saveValue(AppConst.GRID_VIEW, AppConst.TRUE)
            } else {
                showGridViews(false)
                repository.saveValue(AppConst.GRID_VIEW, AppConst.FALSE)
            }

        }

        setting_subMenu.setOnCheckedChangeListener { _, isChecked ->


            if (isChecked) {
                repository.saveValue(AppConst.SUB_MENU, AppConst.TRUE)
            } else {
                repository.saveValue(AppConst.SUB_MENU, AppConst.FALSE)
            }

        }

        setting_cbTime.setOnCheckedChangeListener { _, isChecked ->

            repository.saveValue(AppConst.SHOW_TIME, isChecked)

        }

        setting_cbCal.setOnCheckedChangeListener { _, isChecked ->

            repository.saveValue(AppConst.SHOW_CAL, isChecked)

        }

        setting_btnPassword.setOnClickListener {
            showChangeDialog("Enter New Password", 1)
        }

        setting_btnApiUrl.setOnClickListener {
            showChangeDialog("Enter New Api Url", 2)
        }

        setting_btnSetLogoUrl.setOnClickListener {
            showChangeDialog("Enter New Logo Url", 3)
        }

        setting_btnGallery.setOnClickListener {
            EasyImage.openGallery(this, 1)
        }

        setting_colorBg.setOnClickListener {

            showColorDialog { colorInt ->
                it.setBackgroundColor(colorInt)
                colorIntBg = colorInt
                repository.saveValue(AppConst.COLOR_INT_BG, colorIntBg!!)

            }

        }

        setting_colorText.setOnClickListener {

            showColorDialog { colorInt ->
                it.setBackgroundColor(colorInt)
                colorIntDetailText = colorInt
                repository.saveValue(AppConst.COLOR_INT_TITLE_TEXT, colorIntDetailText!!)
            }

        }

        setting_colorTextTitle.setOnClickListener {

            showColorDialog { colorInt ->
                it.setBackgroundColor(colorInt)
                colorIntTitleText = colorInt
                repository.saveValue(AppConst.COLOR_INT_TITLE_TEXT, colorIntTitleText!!)

            }

        }

        setting_btnAppCurrency.setOnClickListener {

            val curId = sp.getInt(AppConst.CURRENCY, 0)
            val dialog = CurrencyDialog.newInstance(curId)
            dialog.show(supportFragmentManager, "curdialog")
        }


        //Main Background

        setting_rgMain.setOnCheckedChangeListener { _, checkedId ->


            when(checkedId) {

                setting_rbSetBgColor.id -> {
                    showHideMainBgColor(true)
                    repository.saveValue(AppConst.IS_MAIN_IMAGE, false)
                }

                setting_rbSetBgImage.id -> {
                    showHideMainBgColor(false)
                    repository.saveValue(AppConst.IS_MAIN_IMAGE, true)
                }

            }

        }


        setting_btnBgGallery.setOnClickListener {
            EasyImage.openGallery(this, 2)
        }

        setting_btnSetBgUrl.setOnClickListener {
            showChangeDialog("Enter Background Image Url", 4)
        }



        setting_mainBgColor.setOnClickListener {

            showColorDialog { colorInt ->
                it.setBackgroundColor(colorInt)
                colorIntTitleText = colorInt
                repository.saveValue(AppConst.COLOR_MAIN_BG, colorIntTitleText!!)

            }

        }


        setting_startBtnColor.setOnClickListener {

            showColorDialog { colorInt ->
                it.setBackgroundColor(colorInt)
                colorIntTitleText = colorInt
                repository.saveValue(AppConst.COLOR_START_BTN, colorIntTitleText!!)

            }

        }

        setting_startBtnTxtColor.setOnClickListener {

            showColorDialog { colorInt ->
                it.setBackgroundColor(colorInt)
                colorIntTitleText = colorInt
                repository.saveValue(AppConst.COLOR_START_BTN_TXT, colorIntTitleText!!)

            }

        }

        setting_btnBack.setOnClickListener {
            finish()
        }

    }


    private fun showHideMainBgColor(bool: Boolean) {

        if (bool) {
            setting_llMainBgImage.gone()
            setting_llMainBgColor.show()
        } else {
            setting_llMainBgImage.show()
            setting_llMainBgColor.gone()
        }

    }

    override fun onItemClick(id: Int, name: String) {
        repository.saveValue(AppConst.CURRENCY, id)
        setting_appCurrency.text = name
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, object : DefaultCallback() {
            override fun onImagePicked(imageFile: File?, source: EasyImage.ImageSource?, type: Int) {

                if (type == 1) {
                    showImage(imageFile?.absolutePath!!)
                } else if (type == 2) {
                    showBgImage(imageFile?.absolutePath!!)
                }

            }

            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
                //Some error handling
                e?.printStackTrace()
            }

        })
    }

    private fun showChangeDialog(title: String, type: Int) {
        SettingDialog().apply {
            arguments = Bundle().apply {
                putString("title", title)
                putInt("type", type)
            }
            show(supportFragmentManager, "setting_dialog")
        }
    }


    override fun onChangePassword(value: String) {

        sp.edit().apply {
            putString(AppConst.PASSWORD_KEY, value)
        }.apply()

    }

    override fun onChangeApiUrl(value: String) {

        sp.edit().apply {
            putString(AppConst.BASE_URL_KEY, value)
        }.apply()

    }

    override fun onChangeLogoUrl(value: String) {

        setting_imgProgress.show()

        GlobalScope.launch {


            try {
                val request =  service.downloadImage(value)
                val response = request.await()

                val filename = downloadImage(value, response, "logo")

                runOnUiThread {
                    showImage(filename!!)
                    setting_imgProgress.gone()
                }


            } catch (e: Exception) {
                e.printStackTrace()
                setting_imgProgress.gone()

            }


        }



    }

    override fun onChangeBgUrl(value: String) {

        setting_bg_imgProgress.show()

        GlobalScope.launch {


            try {
                val request =  service.downloadImage(value)
                val response = request.await()

                val filename = downloadImage(value, response, "main_bg")

                runOnUiThread {
                    showBgImage(filename!!)
                    setting_bg_imgProgress.gone()
                }


            } catch (e: Exception) {
                e.printStackTrace()
                setting_bg_imgProgress.gone()

            }


        }

    }

    private fun showImage(filename: String) {

        try {
            GlideApp.with(this).load(filename).into(setting_logo_image)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

        sp.edit().apply {
            putString(AppConst.LOGO_URL_KEY, filename)
        }.apply()

    }

    private fun showBgImage(filename: String) {

        try {
            GlideApp.with(this).load(filename).into(setting_bg_image)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

        sp.edit().apply {
            putString(AppConst.BG_URL_KEY, filename)
        }.apply()

    }

    private fun downloadImage(url: String, response: ResponseBody, name: String): String? {

        name.replace(" ", "_")
        name.replace("/", "")

        try {

            val bytes  = response.bytes()
            val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
            val timeStamp = System.currentTimeMillis().toString()
            val filename = "$name-$timeStamp.jpg"

            Log.d("IMAGE_URL", url)
            Log.d("IMAGE_FILENAME", filename)

            return saveImage(bitmap, filename)!!


        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }

    }

    private fun saveImage(image: Bitmap, imageFileName: String): String? {
        var savedImagePath: String? = null

        val storageDir = File("${applicationInfo.dataDir}/pictures")


        var success = true
        if (!storageDir.exists()) {
            success = storageDir.mkdirs()
        }
        if (success) {
            val imageFile = File(storageDir, imageFileName)
            savedImagePath = imageFile.absolutePath
            try {
                val fOut = FileOutputStream(imageFile)
                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                fOut.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        return savedImagePath
    }


    private fun showColorDialog(onColorChange: (colorInt: Int) -> Unit) {

        AmbilWarnaDialog(this, Color.WHITE, false, object : AmbilWarnaDialog.OnAmbilWarnaListener {
            override fun onCancel(dialog: AmbilWarnaDialog?) {
            }

            override fun onOk(dialog: AmbilWarnaDialog?, color: Int) {
                onColorChange(color)
            }

        }).show()

    }


    private fun showGridViews(show: Boolean) {


        if (show) {

            setting_llBgColor.show()
//            setting_llSubMenu.show()
            setting_llShowCal.show()
            setting_llShowTime.show()
            setting_llTextColor.show()
            setting_llTitleTextColor.show()


        } else {

            setting_llBgColor.gone()
//            setting_llSubMenu.gone()
            setting_llShowCal.gone()
            setting_llShowTime.gone()
            setting_llTextColor.gone()
            setting_llTitleTextColor.gone()

        }

    }


}





