package com.samaralii.restaurantapp.features.setting

import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.samaralii.restaurantapp.CurrencyList
import com.samaralii.restaurantapp.R
import kotlinx.android.synthetic.main.dialo_currency.*
import java.lang.Exception

class CurrencyDialog: DialogFragment() {


    companion object {

        private const val ID = "id"

        fun newInstance(id: Int = 0) = CurrencyDialog().apply {
            arguments = Bundle().apply {
                putInt(ID, id)
            }
        }
    }

    private var currId = 0
    private var listener: CurrencyDialogListener? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.getInt(ID)?.let {
            currId = it
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialo_currency, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        curDialog_list.layoutManager = LinearLayoutManager(context)
        val list = CurrencyList()

        list[currId].isSelected = true

        val adapter = CurrencyAdapter(list, context!!, onItemClick = { id, name ->
            listener?.onItemClick(id, name)
            dismiss()
        })

        curDialog_list.adapter = adapter

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        try {
            listener = context as CurrencyDialogListener
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    public interface CurrencyDialogListener{
        fun onItemClick(id: Int, name: String)
    }

}