package com.samaralii.restaurantapp.features.setting

import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.samaralii.restaurantapp.R
import kotlinx.android.synthetic.main.dialog_setting.*

class SettingDialog: DialogFragment() {



    private var title: String? = null
    private var type: Int? = null
    private var listener: SettingDialogListener? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = arguments?.getString("title", "TITLE")
        type = arguments?.getInt("type")
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_setting, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        settingDialog_title.text = title

        settingDialog_btnDone.setOnClickListener {

            if (settingDialog_et.text.toString().isNotEmpty()) {
               onChangeValue(settingDialog_et.text.toString())
            }


            dialog.dismiss()

        }
    }


    private fun onChangeValue(value: String) {
        when(type) {
            1 -> listener?.onChangePassword(value)
            2 -> listener?.onChangeApiUrl(value)
            3 -> listener?.onChangeLogoUrl(value)
            4 -> listener?.onChangeBgUrl(value)
        }
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as SettingDialogListener
    }

    interface SettingDialogListener{
        fun onChangePassword(value: String)
        fun onChangeApiUrl(value: String)
        fun onChangeLogoUrl(value: String)
        fun onChangeBgUrl(value: String)
    }

}