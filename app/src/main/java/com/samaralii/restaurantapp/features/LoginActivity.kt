package com.samaralii.restaurantapp.features

import android.content.Intent
import android.os.Bundle
import com.samaralii.restaurantapp.AppConst
import com.samaralii.restaurantapp.BaseActivity
import com.samaralii.restaurantapp.R
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
    }

    private fun init() {

        login_btnBack.setOnClickListener { finish() }
        login_btnLogin.setOnClickListener {
            loginUser()
        }

        login_btnSettings.setOnClickListener {
            startActivity(Intent(this@LoginActivity, SetUrlActivity::class.java))
        }


    }


    fun loginUser() {


        if (login_etEmail.text.toString().isEmpty() || login_etEmail.text.toString() != "admin"){
            login_etEmail.error = ""
            return
        } else {
            login_etEmail.error = null
        }


        val password = sp.getString(AppConst.PASSWORD_KEY, "")

        if (login_etPassword.text.toString().isEmpty() || login_etPassword.text.toString() != password){
            login_etPassword.error = ""
            return
        } else {
            login_etPassword.error = null
        }


        startActivity(Intent(this@LoginActivity, SyncActivity::class.java))
        finish()

    }
}