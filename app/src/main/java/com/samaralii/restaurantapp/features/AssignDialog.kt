package com.samaralii.restaurantapp.features

import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.samaralii.restaurantapp.R
import kotlinx.android.synthetic.main.dialog_assign.*

class AssignDialog: DialogFragment() {

    var listener: AssignDialogListener? = null

    companion object {
        fun newInstance() = AssignDialog().apply {

        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as AssignDialogListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_assign, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        assign_btnStart.setOnClickListener {
            listener?.onAssignClick()
        }

        assign_btnCancel.setOnClickListener { dismiss() }

    }


    interface AssignDialogListener{
        fun onAssignClick()
    }

}