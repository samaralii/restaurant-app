package com.samaralii.restaurantapp.features

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import com.samaralii.restaurantapp.*
import com.samaralii.restaurantapp.adapters.MenuItemAdapter
import com.samaralii.restaurantapp.data.models.SubMenu
import com.samaralii.restaurantapp.data.models.SubMenuItem
import kotlinx.android.synthetic.main.activity_menu.*
import java.util.*

class MenuActivity: BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        init()
    }

    private fun init() {

        val sp = Provider.provideSharedPreference(this)
        val whiteColor = Color.WHITE
        val colorInt = sp.getInt(AppConst.COLOR_INT_BG, whiteColor)

        val blackColor = Color.BLACK
        val textColorInt = sp.getInt(AppConst.COLOR_INT_TITLE_TEXT, blackColor)

        menu_tvTitle.setTextColor(textColorInt)

        menu_bg.setBackgroundColor(colorInt)

        login_btnBack.setOnClickListener { finish() }

        val menuList = repository.getData()

        if (menuList == null || menuList.isEmpty()) {
            toast("No Data")
            return
        }

        Log.d("DATA", menuList.toString())

        sub_menu_list.layoutManager = GridLayoutManager(this, 3)
        sub_menu_list.adapter = MenuItemAdapter(menuList, this, onItemClick = { items, pos ->


            if (sp.getInt(AppConst.SUB_MENU, 0) == 1) {


                startActivity(Intent(this@MenuActivity, SubMenuItemsActivity::class.java).apply {
//                    putParcelableArrayListExtra("data", showMenuItems(pos) as ArrayList<SubMenuItem>)

                    putExtra("data", repository.saveSubMenuItems(showMenuItems(pos)))

                })


            } else if (sp.getInt(AppConst.SUB_MENU, 0) == 2) {

                startActivity(Intent(this@MenuActivity, SubMenuActivity::class.java).apply {
//                    putParcelableArrayListExtra("data", items as ArrayList<SubMenu>)
                    putExtra("data", repository.saveSubMenu(items))

                })

            }
            


        })


    }

    private fun showMenuItems(pos: Int): List<SubMenuItem> {

        repository.getData()?.get(pos)?.subcategories?.let { list ->

            val itemsList: MutableList<SubMenuItem> = arrayListOf()


            for (i in list) {

                for (j in i.items){

                    itemsList.add(j)

                }

            }

            return itemsList

        }


    }


}