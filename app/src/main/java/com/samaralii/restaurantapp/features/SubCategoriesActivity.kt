package com.samaralii.restaurantapp.features

import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.samaralii.restaurantapp.*
import com.samaralii.restaurantapp.adapters.NewSubMenuAdapter
import com.samaralii.restaurantapp.data.models.SubMenuItem
import kotlinx.android.synthetic.main.activity_subcategories.*


class SubCategoriesActivity: BaseActivity() {

    private var subMenuAdapter: NewSubMenuAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subcategories)
        init()
    }

    private fun init() {

        val menuTitleFont = Typeface.createFromAsset(assets, "fonts/viner_hand_itc.ttf")
        val subMenuTitle = Typeface.createFromAsset(assets, "fonts/baskerville_bold.ttf")
        val subMenuDes = Typeface.createFromAsset(assets, "fonts/gautami.ttf")

        val list = repository.getSubMenuItems(intent.getStringExtra("data"))
        val title = intent.getStringExtra("title")
        subCat_mainTitle.typeface = menuTitleFont
        subCat_mainTitle.text = title

        val symbol = CurrencyList()[sp.getInt(AppConst.CURRENCY, 0)].symbol


        subMenuAdapter = NewSubMenuAdapter(desFont = subMenuDes, titleFont = subMenuTitle ,context = applicationContext, onItemClick = {
            showPreview(it)
        }, curSymbol = symbol)


        subCat_showPreview.setOnClickListener {
            hidePreview()
        }



        subCat_listSubMenu.layoutManager = LinearLayoutManager(this)
        subCat_listSubMenu.adapter = subMenuAdapter


        subMenuAdapter?.addData(list as MutableList<SubMenuItem>)

        subCat_btnBack.setOnClickListener {
            finish()
        }
    }

    private fun showPreview(imageUrl: String) {

       subCat_showPreview.show()
        GlideApp.with(this).load(imageUrl).into(subCat_ivImage)


    }

    private fun hidePreview() {
        subCat_showPreview.gone()
    }


}