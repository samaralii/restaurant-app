package com.samaralii.restaurantapp.features.main

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import com.samaralii.restaurantapp.*
import com.samaralii.restaurantapp.features.AssignDialog
import com.samaralii.restaurantapp.features.LoginActivity
import com.samaralii.restaurantapp.features.MenuActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() , AssignDialog.AssignDialogListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {

        if (repository.getBaseUrl() == null) {
            repository.addBaseUrl(AppConst.DEFAULT_BASE_URL)
        }

        if (sp.getString(AppConst.PASSWORD_KEY, null) == null) {
            sp.edit().apply {
                putString(AppConst.PASSWORD_KEY, "letmein")
            }.apply()
        }

        val styleFlag = sp.getInt(AppConst.GRID_VIEW, 0)
        if(styleFlag == 0) {
            repository.saveValue(AppConst.GRID_VIEW, AppConst.FALSE)
        }

        val subMenu = sp.getInt(AppConst.SUB_MENU, 0)
        if(styleFlag == 0) {
            repository.saveValue(AppConst.SUB_MENU, AppConst.TRUE)
        }


        if (sp.getString(AppConst.LOGO_URL_KEY, null) != null) {

            try {
                GlideApp.with(this).load(sp.getString(AppConst.LOGO_URL_KEY, "")).into(imageView5)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }


        } else {
            imageView5.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.hub))
        }





        main_btnMenu.setOnClickListener {
            openMenuActivity()
        }

        main_btnLogin.setOnClickListener {
            startActivity(Intent(this@MainActivity, LoginActivity::class.java))
        }

        main_btnAssign.setOnClickListener {
            val dialog = AssignDialog.newInstance()
            dialog.show(supportFragmentManager, "assign_dialog")
        }

        startAnimation()

    }

    override fun onResume() {
        super.onResume()

        if (sp.getBoolean(AppConst.IS_MAIN_IMAGE, false)) {

            //show bg image
            try {
                GlideApp.with(this).load(sp.getString(AppConst.BG_URL_KEY, "")).into(main_bgImage)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }

            main_bgColor.gone()
            main_bgImage.show()

        } else {

            val whiteColor = Color.WHITE
            val bg = sp.getInt(AppConst.COLOR_MAIN_BG, whiteColor)

            main_bgColor.setBackgroundColor(bg)


            main_bgColor.show()
            main_bgImage.gone()

        }


        //start button colors

        val blackColor = Color.BLACK
        val bg = sp.getInt(AppConst.COLOR_START_BTN, blackColor)


        DrawableCompat.setTint(main_btnMenu.background, bg)

        val whiteColor = Color.WHITE
        val txtColor = sp.getInt(AppConst.COLOR_START_BTN_TXT, whiteColor)

        main_btnStart.setTextColor(txtColor)

    }

    private fun startAnimation() {


//        YoYo.with(Techniques.Pulse)
//                .duration(2000)
//                .repeat(500)
//                .playOn(main_btnMenu)

    }


    override fun onAssignClick() {
        startActivity(Intent(this@MainActivity, MainActivity::class.java))
        finish()
    }


    private fun openMenuActivity() {

        val gridInt = sp.getInt(AppConst.GRID_VIEW, 0)

        if (gridInt == 1) {
            startActivity(Intent(this@MainActivity, NewMenuActivity::class.java))
        } else {
            startActivity(Intent(this@MainActivity, MenuActivity::class.java))
        }

    }
}
