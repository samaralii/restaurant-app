package com.samaralii.restaurantapp.features.detail

import android.graphics.Color
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.method.ScrollingMovementMethod
import com.samaralii.restaurantapp.*
import com.samaralii.restaurantapp.adapters.DetailOptionsAdapter
import com.samaralii.restaurantapp.data.models.Options
import kotlinx.android.synthetic.main.activity_detail_new.*

class DetailActivity: BaseActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_new)
        init()
    }

    private fun init() {

        val sp = Provider.provideSharedPreference(this)
        val blackColor = Color.BLACK
        val colorInt = sp.getInt(AppConst.COLOR_INT_BG, blackColor)

        val whiteColor = Color.WHITE
        val textColorInt = sp.getInt(AppConst.COLOR_INT_DETAIL_TEXT, whiteColor)
        changeTextColors(textColorInt)

        detail_description.movementMethod = ScrollingMovementMethod()

        detail_bg.setBackgroundColor(colorInt)

        detail_btnBack.setOnClickListener { finish() }

        val pos = intent.getIntExtra("pos",0)
        val list = repository.getSubMenuItems(intent.getStringExtra("data"))
        val data = list[pos]


        initOptionList(data.options, textColorInt)

        data.filename?.let {
            GlideApp.with(this).load(it).into(detail_mainImage)
        }

        detail_name.text = data.name
        detail_description.text = data.description

        val symbol = CurrencyList()[sp.getInt(AppConst.CURRENCY, 0)].symbol


        detail_tvPrice.text = "$symbol ${data.price}"
        detail_tvCal.text = data.calories
        detail_tvTime.text = data.preparation_time


        val showCal = Provider.provideSharedPreference(this).getBoolean(AppConst.SHOW_CAL, true)
        if (showCal) {

            detail_ivCal.show()
            detail_tvCal.show()

        } else {

            detail_ivCal.hide()
            detail_tvCal.hide()

        }

        val showTime = Provider.provideSharedPreference(this).getBoolean(AppConst.SHOW_TIME, true)
        if (showTime) {

            detail_ivTime.show()
            detail_tvTime.show()

        } else {

            detail_ivTime.hide()
            detail_tvTime.hide()

        }

    }


    fun changeTextColors(color: Int) {
        detail_tvTime.setTextColor(color)
        detail_tvCal.setTextColor(color)
        detail_name.setTextColor(color)
        detail_tvPrice.setTextColor(color)
        detail_description.setTextColor(color)
        detail_tvOptions.setTextColor(color)
    }

    private fun initOptionList(data: List<Options>?, colorInt: Int) {

        if (!data.isNullOrEmpty()) {

            detail_tvOptions.show()
            detail_listoptions.show()
            val symbol = CurrencyList()[sp.getInt(AppConst.CURRENCY, 0)].symbol


            detail_listoptions.layoutManager = LinearLayoutManager(this)
            detail_listoptions.adapter = DetailOptionsAdapter(data, colorInt, symbol)
        } else {
            detail_tvOptions.gone()
            detail_listoptions.gone()
        }

    }

}