package com.samaralii.restaurantapp.features

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import com.samaralii.restaurantapp.*
import com.samaralii.restaurantapp.adapters.SubMenuItemAdapter
import com.samaralii.restaurantapp.data.models.SubMenuItem
import com.samaralii.restaurantapp.features.detail.DetailActivity
import kotlinx.android.synthetic.main.activity_sub_menu_item.*
import java.util.ArrayList

class SubMenuItemsActivity: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub_menu_item)
        init()
    }

    private fun init() {

        val sp = Provider.provideSharedPreference(this)
        val whiteColor = Color.WHITE
        val colorInt = sp.getInt(AppConst.COLOR_INT_BG, whiteColor)


        val blackColor = Color.BLACK
        val textColorInt = sp.getInt(AppConst.COLOR_INT_TITLE_TEXT, blackColor)

        menu_tvTitle.setTextColor(textColorInt)
        menu_subtitle.setTextColor(textColorInt)

        sub_menu_item_bg.setBackgroundColor(colorInt)


        sub_menu_item_btnBack.setOnClickListener { finish() }

//        val list: ArrayList<SubMenuItem> = intent.getParcelableArrayListExtra("data")

        val data = intent.getStringExtra("data")
        val list = repository.getSubMenuItems(data)

        val symbol = CurrencyList()[sp.getInt(AppConst.CURRENCY, 0)].symbol

        sub_menu__item_list.layoutManager = GridLayoutManager(this, 3)
        sub_menu__item_list.adapter = SubMenuItemAdapter(list, this, onItemClick = { _, pos ->
            startActivity(Intent(this@SubMenuItemsActivity, DetailActivity::class.java).apply {
                putExtra("pos", pos)
                putExtra("data", repository.saveSubMenuItems(list))
            })
        }, curSymbol = symbol)

    }


}