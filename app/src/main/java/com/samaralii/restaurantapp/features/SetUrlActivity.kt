package com.samaralii.restaurantapp.features

import android.os.Bundle
import com.samaralii.restaurantapp.BaseActivity
import com.samaralii.restaurantapp.R
import com.samaralii.restaurantapp.toast
import kotlinx.android.synthetic.main.activity_seturl.*

class SetUrlActivity: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seturl)
        init()
    }

    private fun init() {
        setUrl_btnBack.setOnClickListener { finish() }
        setUrl_btnChange.setOnClickListener { onBtnChange() }


        repository.getBaseUrl().let {
            setUrl_etUrl.setText(it)
        }


    }

    private fun onBtnChange() {
        if (setUrl_etUrl.text.isEmpty()) {
            setUrl_etUrl.error = "Required Field"
            return
        } else {
            setUrl_etUrl.error = null
        }

        repository.addBaseUrl(setUrl_etUrl.text.toString())
        toast("Url Successfully saved")
    }

}