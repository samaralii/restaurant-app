package com.samaralii.restaurantapp.features

import android.content.Intent
import android.os.Bundle
import com.samaralii.restaurantapp.BaseActivity
import com.samaralii.restaurantapp.R
import com.samaralii.restaurantapp.features.main.MainActivity
import kotlinx.android.synthetic.main.activity_assign.*
import android.content.ComponentName
import android.app.admin.DevicePolicyManager
import android.os.Build
import android.support.annotation.RequiresApi
import android.util.Log
import com.samaralii.restaurantapp.AppConst


class AssignActivity: BaseActivity(), AssignDialog.AssignDialogListener {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assign)
        init()
    }

    private fun init() {

        if (repository.getBaseUrl() == null) {
            repository.addBaseUrl(AppConst.DEFAULT_BASE_URL)
        }

        if (sp.getString(AppConst.PASSWORD_KEY, null) == null) {
            sp.edit().apply {
                putString(AppConst.PASSWORD_KEY, "letmein")
            }.apply()
        }

        val styleFlag = sp.getInt(AppConst.GRID_VIEW, 0)
        if(styleFlag == 0) {
            repository.saveValue(AppConst.GRID_VIEW, AppConst.FALSE)
        }

        val subMenu = sp.getInt(AppConst.SUB_MENU, 0)
        if(styleFlag == 0) {
            repository.saveValue(AppConst.SUB_MENU, AppConst.TRUE)
        }



        assign_btnStart.setOnClickListener {

        }

        assign_btnLogin.setOnClickListener {
            startActivity(Intent(this@AssignActivity, LoginActivity::class.java))

        }

        assign_btnAssign.setOnClickListener {

            val dialog = AssignDialog.newInstance()
            dialog.show(supportFragmentManager, "assign_dialog")

        }


        assign_iv.setOnClickListener {

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                lockDevice()
//            }

        }


    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun lockDevice() {

        val mDPM = getSystemService(DEVICE_POLICY_SERVICE) as DevicePolicyManager
        val mDeviceAdminSample = ComponentName(this, AssignActivity::class.java)

        if (mDPM.isDeviceOwnerApp(this.packageName)) {
            Log.d("TAG", "isDeviceOwnerApp: YES")
            val packages = arrayOf(this.packageName)
            mDPM.setLockTaskPackages(mDeviceAdminSample, packages)
        } else {
            Log.d("TAG", "isDeviceOwnerApp: NO")
        }

        if (mDPM.isLockTaskPermitted(this.packageName)) {
            Log.d("TAG", "isLockTaskPermitted: ALLOWED")
            startLockTask()
        } else {
            Log.d("TAG", "isLockTaskPermitted: NOT ALLOWED")
        }

    }

    override fun onAssignClick() {
        startActivity(Intent(this@AssignActivity, MainActivity::class.java))
        finish()
    }

}