package com.samaralii.restaurantapp.features.setting

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.preference.EditTextPreference
import android.support.v7.preference.PreferenceFragmentCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.samaralii.restaurantapp.AppConst
import com.samaralii.restaurantapp.Provider
import com.samaralii.restaurantapp.R

class FragmentPreference: PreferenceFragmentCompat() {


    private val sp by lazy { Provider.provideSharedPreference(context!!) }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.setting_preference)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return super.onCreateView(inflater, container, savedInstanceState).apply {
            this!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.app_bg))
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }


    private fun init() {


        val prefPassword = preferenceManager.findPreference("prefPassword") as EditTextPreference
        val prefLogoUrl = preferenceManager.findPreference("prefLogoUrl") as EditTextPreference
        val prefApiUrl = preferenceManager.findPreference("prefApiUrl") as EditTextPreference






        val apiUrl = sp.getString(AppConst.BASE_URL_KEY, AppConst.BASE_URL)
        prefApiUrl.summary = apiUrl

        prefApiUrl.setOnPreferenceChangeListener { preference, newValue ->

            preference.summary = newValue.toString()
            sp.edit().apply {
                putString(AppConst.BASE_URL_KEY, newValue.toString())
            }.apply()

            true
        }









    }



}