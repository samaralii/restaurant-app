package com.samaralii.restaurantapp.features

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import com.samaralii.restaurantapp.*
import com.samaralii.restaurantapp.data.models.MenuItem
import com.samaralii.restaurantapp.features.setting.NewSettingActivity
import kotlinx.android.synthetic.main.activity_sync.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import java.io.File
import java.io.FileOutputStream

class SyncActivity: BaseActivity() {

    private var totalImageCount = 0
    private var completedImageCount = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sync)
        init()
    }

    private fun init() {

        onNotSyncingView()

        sync_btnBack.setOnClickListener { finish() }

        sync_btnSync.setOnClickListener { onSyncBtn() }

        sync_btnSetting.setOnClickListener {
            startActivity(Intent(this@SyncActivity, NewSettingActivity::class.java))
        }

        sync_btnSetUrl.setOnClickListener {
            startActivity(Intent(this@SyncActivity, SetUrlActivity::class.java))

        }

    }


    private fun onSyncBtn() {


        val url = repository.getBaseUrl()


        if (url == null) {
            toast("No Url Found")
            return
        }

        onSyncingView()



        GlobalScope.launch {

            try {

                val request = service.getAllData(url)
                val response = request.await()

                if (response.isSuccessful) {
                    Log.d("DATA", response.body().toString())

                    if (response.body() != null && response.body()!!.items!!.isNotEmpty()) {
                        extractImages(response.body()?.items!!)
                    } else {
                        runOnUiThread {
                            onNotSyncingView()
                            toast("Error")
                        }
                    }

                } else {
                    Log.e("ERROR", "${response.code()}")
                    runOnUiThread {
                        onNotSyncingView()
                        toast("Error")
                    }
                }

            } catch (e: Exception) {
                Log.e("DATA ALL", "ERROR", e)
                runOnUiThread {
                    onNotSyncingView()
                    toast("Error")
                }
            }
        }
    }


    private fun updateProgress() {
        completedImageCount++


        val totalImages = totalImageCount
        val imagesDoneDownloading = completedImageCount

        val a: Double = 100 / totalImages.toDouble()
        val b: Double = a * imagesDoneDownloading.toDouble()

        sync_tvStatus.text = "${b.toInt()}%"


        sync_tvProgress.progress = completedImageCount
        Log.d("PROGRESS","Completed : $completedImageCount")
    }

    private fun extractImages(data: List<MenuItem>) {


        GlobalScope.launch {
            totalImageCount += data.size
            for (i in data){
                totalImageCount += i.subcategories.size
                for (j in i.subcategories) {
                    totalImageCount += j.items.size
                }
            }

            Log.d("PROGRESS", "Total : $totalImageCount")


            runOnUiThread {
                sync_tvProgress.max = totalImageCount
            }


            for (i in data) {


                if (i.picture != null && i.picture!!.isNotEmpty()) {

                    val request = service.downloadImage(i.picture!!)
                    val response = request.await()

                    i.filename = downloadImage(i.picture!!, response, i.name!!)

                    runOnUiThread {
                        updateProgress()
                    }

                }


                if (!i.subcategories.isNullOrEmpty()) {

                    for (j in i.subcategories) {

                        if (j.picture != null && j.picture.isNotEmpty()) {

                            val request = service.downloadImage(j.picture)
                            val response = request.await()

                            j.filename = downloadImage(j.picture, response, j.name)

                            runOnUiThread {
                                updateProgress()
                            }

                        }

                        for (k in j.items!!) {

                            if (k.picture != null && k.picture!!.isNotEmpty()) {

                                val request = service.downloadImage(k.picture!!)
                                val response = request.await()

                                k.filename = downloadImage(k.picture!!, response, k.name!!)

                                runOnUiThread {
                                    updateProgress()
                                }


                            }
                        }
                    }

                }


            }


            repository.saveData(data)


            runOnUiThread {
                onNotSyncingView()
                toast("Successfully Synced")
                startActivity(intent)
                finish()
            }


        }

    }


    private fun downloadImage(url: String, response: ResponseBody, name: String): String? {

        name.replace(" ", "_")
        name.replace("/", "")

        try {

            val bytes  = response.bytes()
            val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
            val timeStamp = System.currentTimeMillis().toString()
            val filename = "$name-$timeStamp.jpg"

            Log.d("IMAGE_URL", url)
            Log.d("IMAGE_FILENAME", filename)

            return saveImage(bitmap, filename)!!


        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }

    }

    private fun saveImage(image: Bitmap, imageFileName: String): String? {
        var savedImagePath: String? = null

        val storageDir = File("${applicationInfo.dataDir}/pictures")


        var success = true
        if (!storageDir.exists()) {
            success = storageDir.mkdirs()
        }
        if (success) {
            val imageFile = File(storageDir, imageFileName)
            savedImagePath = imageFile.absolutePath
            try {
                val fOut = FileOutputStream(imageFile)
                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                fOut.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            // Add the image to the system gallery
        }
        return savedImagePath
    }


    private fun onSyncingView() {
        sync_tvStatus.text = "Syncing..."
        sync_tvProgress.show()
    }

    private fun onNotSyncingView() {
        sync_tvStatus.text = "Sync Data"
        sync_tvProgress.hide()
    }



}