package com.samaralii.restaurantapp.features

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import com.samaralii.restaurantapp.AppConst
import com.samaralii.restaurantapp.BaseActivity
import com.samaralii.restaurantapp.Provider
import com.samaralii.restaurantapp.R
import com.samaralii.restaurantapp.adapters.SubMenuAdapter
import com.samaralii.restaurantapp.data.models.SubMenu
import com.samaralii.restaurantapp.data.models.SubMenuItem
import kotlinx.android.synthetic.main.activity_sub_menu.*
import java.util.ArrayList

class SubMenuActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub_menu)
        init()
    }

    private fun init() {

        val sp = Provider.provideSharedPreference(this)
        val whiteColor = Color.WHITE
        val colorInt = sp.getInt(AppConst.COLOR_INT_BG, whiteColor)

        val blackColor = Color.BLACK
        val textColorInt = sp.getInt(AppConst.COLOR_INT_TITLE_TEXT, blackColor)

        menu_tvTitle.setTextColor(textColorInt)
        menu_subtitle.setTextColor(textColorInt)

        sub_menu_bg.setBackgroundColor(colorInt)

//        val list: ArrayList<SubMenu> = intent.getParcelableArrayListExtra("data")

        val data = intent.getStringExtra("data")
        val list = repository.getSubMenu(data)


        login_btnBack.setOnClickListener { finish() }


        sub_menu_list.layoutManager = GridLayoutManager(this, 3)
        sub_menu_list.adapter = SubMenuAdapter(list, this, onItemClick = {
            startActivity(Intent(this@SubMenuActivity, SubMenuItemsActivity::class.java).apply {
//                putParcelableArrayListExtra("data", it as ArrayList<SubMenuItem>)

                putExtra("data", repository.saveSubMenuItems(it))

            })
        })

    }

}