package com.samaralii.restaurantapp.features

import android.graphics.Color
import android.os.Bundle
import com.samaralii.restaurantapp.*
import kotlinx.android.synthetic.main.activity_newsetting.*
import kotlinx.android.synthetic.main.activity_setting.*
import yuku.ambilwarna.AmbilWarnaDialog

class SettingActivity: BaseActivity() {

    var colorIntBg: Int? = null
    var colorIntDetailText: Int? = null
    var colorIntTitleText: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        init()
    }

    private fun init() {


        val showCal = sp.getBoolean(AppConst.SHOW_CAL, true)
        setting_cbShowCal.isChecked = showCal

        val showTime = sp.getBoolean(AppConst.SHOW_TIME, true)
        setting_cbShowTime.isChecked = showTime

        val bgColor = sp.getInt(AppConst.COLOR_INT_BG, Color.WHITE)
        setting_colorPickerBg.setBackgroundColor(bgColor)

        val detailTextColor = sp.getInt(AppConst.COLOR_INT_DETAIL_TEXT, Color.WHITE)
        setting_colorPickerDetailText.setBackgroundColor(detailTextColor)

        val menuTitleColor = sp.getInt(AppConst.COLOR_INT_TITLE_TEXT, Color.BLACK)
        setting_colorPickerMenuTitle.setBackgroundColor(menuTitleColor)






//        setting_btnBack.setOnClickListener { finish() }


        setting_colorPickerBg.setOnClickListener {

            showColorDialog {color ->
                it.setBackgroundColor(color)
                colorIntBg = color
            }
        }

        setting_colorPickerMenuTitle.setOnClickListener {

            showColorDialog {color ->
                it.setBackgroundColor(color)
                colorIntTitleText = color
            }
        }

        setting_colorPickerDetailText.setOnClickListener {

            showColorDialog {color ->
                it.setBackgroundColor(color)
                colorIntDetailText = color
            }
        }


        setting_btnSave.setOnClickListener {

            colorIntBg?.let { value ->
                repository.saveValue(AppConst.COLOR_INT_BG, value)
            }

            colorIntDetailText?.let { value ->
                repository.saveValue(AppConst.COLOR_INT_DETAIL_TEXT, value)
            }

            colorIntTitleText?.let { value ->
                repository.saveValue(AppConst.COLOR_INT_TITLE_TEXT, value)
            }


            repository.saveValue(AppConst.SHOW_CAL, setting_cbShowCal.isChecked)
            repository.saveValue(AppConst.SHOW_TIME, setting_cbShowTime.isChecked)

            toast("Successfully Saved")
        }


    }


    private fun showColorDialog(onColorChange: (colorInt: Int) -> Unit) {

        AmbilWarnaDialog(this, Color.WHITE, false, object : AmbilWarnaDialog.OnAmbilWarnaListener {
            override fun onCancel(dialog: AmbilWarnaDialog?) {
            }

            override fun onOk(dialog: AmbilWarnaDialog?, color: Int) {
                onColorChange(color)
            }

        }).show()

    }




}