package com.samaralii.restaurantapp.features.setting

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.samaralii.restaurantapp.R
import com.samaralii.restaurantapp.data.models.Currency

class CurrencyAdapter(private val data: List<Currency>,
                      private val context: Context,
                      private val onItemClick: (Int, String) -> Unit
                      ): RecyclerView.Adapter<CurrencyVH>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.currency_list, parent, false)
        return CurrencyVH(v)
    }

    override fun onBindViewHolder(holder: CurrencyVH, position: Int) {

        val cur = data[position]

        if (cur.isSelected) {
            holder.bg.setBackgroundColor(ContextCompat.getColor(context, R.color.bg_selected))
        } else {
            holder.bg.setBackgroundColor(ContextCompat.getColor(context, R.color.app_bg))
        }


        holder.name.text = cur.name
        holder.symbol.text = cur.symbol

        holder.itemView.setOnClickListener {
            onItemClick.invoke(cur.id, "${cur.name} ${cur.symbol}")
        }

    }


    override fun getItemCount() = data.size

}


class CurrencyVH(v: View): RecyclerView.ViewHolder(v){
    val name by lazy { v.findViewById<TextView>(R.id.listcurrency_name) }
    val symbol by lazy { v.findViewById<TextView>(R.id.listcurrency_symbol) }
    val bg by lazy { v.findViewById<LinearLayout>(R.id.listcurrency_bg) }
}