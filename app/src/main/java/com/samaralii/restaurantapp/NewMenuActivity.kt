package com.samaralii.restaurantapp

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.samaralii.restaurantapp.adapters.NewMenuItemAdapter
import com.samaralii.restaurantapp.adapters.NewSubMenuAdapter
import com.samaralii.restaurantapp.adapters.SubCategoriesAdapter
import com.samaralii.restaurantapp.data.models.SubMenu
import com.samaralii.restaurantapp.data.models.SubMenuItem
import com.samaralii.restaurantapp.features.SubCategoriesActivity
import kotlinx.android.synthetic.main.activity_newmenu.*

class NewMenuActivity: BaseActivity() {

    private var subMenuAdapter: NewSubMenuAdapter? = null
    private var subCategoriesAdapter: SubCategoriesAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_newmenu)
        init()
    }

    private fun init() {


        val menuTitleFont = Typeface.createFromAsset(assets, "fonts/viner_hand_itc.ttf")
        val subMenuTitle = Typeface.createFromAsset(assets, "fonts/baskerville_bold.ttf")
        val subMenuDes = Typeface.createFromAsset(assets, "fonts/gautami.ttf")


        val symbol = CurrencyList()[sp.getInt(AppConst.CURRENCY, 0)].symbol


        menu_mainTitle.typeface = menuTitleFont


        val menuList = repository.getData()

        if (menuList == null || menuList.isEmpty()) {
            toast("No Data")
            return
        }

        val isSubMenu = sp.getInt(AppConst.SUB_MENU, AppConst.FALSE)



        menu_listMenu.hasFixedSize()
        menu_listMenu.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        val adapter = NewMenuItemAdapter(menuList, menuTitleFont, onItemClick = { pos, title ->
            menu_mainTitle.text = title

            if (isSubMenu == AppConst.TRUE) {
                showSubMenuItems(pos)
            } else {
                showMenuItems(pos)
            }

        })

        menu_listMenu.adapter = adapter

        if (isSubMenu == AppConst.TRUE) {


            subCategoriesAdapter = SubCategoriesAdapter(desFont = subMenuDes,
                    titleFont = subMenuTitle ,context = applicationContext, onItemClick = { list, title ->

                startActivity(Intent(this@NewMenuActivity, SubCategoriesActivity::class.java).apply {
                    putExtra("data", repository.saveSubMenuItems(list))
                    putExtra("title", title)
                })

            })


            menu_listSubMenu.layoutManager = LinearLayoutManager(this)
            menu_listSubMenu.adapter = subCategoriesAdapter


        } else {



            //Set initial items list
            menu_mainTitle.text = menuList[0].name
            showMenuItems(0)


            subMenuAdapter = NewSubMenuAdapter(desFont = subMenuDes, titleFont = subMenuTitle ,context = applicationContext, onItemClick = {
                showPreview(it)
            }, curSymbol = symbol)


            menu_showPreview.setOnClickListener {
                hidePreview()
            }



            menu_listSubMenu.layoutManager = LinearLayoutManager(this)
            menu_listSubMenu.adapter = subMenuAdapter


        }

        menu_btnClose.setOnClickListener { hidePreview() }


    }


    private fun showMenuItems(pos: Int) {

        repository.getData()?.get(pos)?.subcategories?.let { list ->

            val itemsList: MutableList<SubMenuItem> = arrayListOf()


            for (i in list) {

                for (j in i.items){

                    itemsList.add(j)

                }

            }

            subMenuAdapter?.addData(itemsList)


        }


    }

    private fun showSubMenuItems(pos: Int) {

        repository.getData()?.get(pos)?.subcategories?.let {
            subCategoriesAdapter?.addData(it as MutableList<SubMenu>)
        }

    }


    private fun showPreview(imageUrl: String) {

        menu_showPreview.show()

        GlideApp.with(this).load(imageUrl).into(menu_ivImage)


    }

    private fun hidePreview() {
        menu_showPreview.gone()
    }



}