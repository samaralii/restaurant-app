package com.samaralii.restaurantapp

import android.support.v7.app.AppCompatActivity
import com.samaralii.restaurantapp.data.repository.Repository

open class BaseActivity: AppCompatActivity() {

    protected val repository by lazy { Repository(Provider.provideSharedPreference(this)) }
    protected val service by lazy { Provider.provideRetrofit().create(Services::class.java) }
    protected val sp by lazy { Provider.provideSharedPreference(this) }



}