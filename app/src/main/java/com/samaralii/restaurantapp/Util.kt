package com.samaralii.restaurantapp

import android.content.Context
import android.view.View
import android.widget.Toast
import com.samaralii.restaurantapp.data.models.Category
import com.samaralii.restaurantapp.data.models.Currency

fun CategoryList() = listOf(
        Category("Dinner", R.drawable.dinner),
        Category("Dinner", R.drawable.dinner),
        Category("Dinner", R.drawable.dinner),
        Category("Dinner", R.drawable.dinner),
        Category("Dinner", R.drawable.dinner),
        Category("Burgers", R.drawable.dinner),
        Category("Burgers", R.drawable.dinner),
        Category("Burgers", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Lunch", R.drawable.dinner),
        Category("Soups", R.drawable.dinner),
        Category("Desserts", R.drawable.dinner),
        Category("Desserts", R.drawable.dinner),
        Category("Desserts", R.drawable.dinner),
        Category("Desserts", R.drawable.dinner),
        Category("Desserts", R.drawable.dinner),
        Category("Drinks", R.drawable.dinner))


fun CurrencyList() = listOf(
        Currency(0, "Dollar", "$", false),
        Currency(1, "AED", "AED", false),
        Currency(2, "PKR", "Rs", false),
        Currency(3, "Euro", "€", false)
)



fun ItemsList(): List<Category> {

    val list: MutableList<Category> = mutableListOf()

    for (i in 0..50) {
        list.add(Category("Item $i", R.drawable.dinner))
    }

    return list

}

fun View.show() {
    this.visibility = View.VISIBLE
}


fun View.hide() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun Context.toast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
}


fun String.getImageUrl(): String {
    return "http://hrspidersystem.com/menu/img/uploads/$this"
}



object AppConst {

    const val BASE_URL = "https://www.google.com"

    //    const val DEFAULT_BASE_URL = "http://hrspidersystem.com/pos1/index.php/api/all"
    const val DEFAULT_BASE_URL = "http://hrspidersystem.com/menu/categories/getmenu/2.json"
    const val DEFAULT_LOGO_URL = "http://i.imgur.com/I86rTVl.jpg"

    const val SHAREDPREFERENCE_NAME = "my_pref"

    const val BASE_URL_KEY = "base_url"
    const val LOGO_URL_KEY = "logo_url"
    const val PASSWORD_KEY = "password"


    const val DATA = "data"
    const val COLOR_INT_BG = "color_int"
    const val SHOW_CAL = "show_cal"
    const val SHOW_TIME = "show_time"

    const val COLOR_INT_DETAIL_TEXT = "detail_color"
    const val COLOR_INT_TITLE_TEXT = "title_color"
    const val GRID_VIEW = "new_style"
    const val SUB_MENU = "sub_menu"
    const val FALSE = 1
    const val TRUE = 2
    const val CURRENCY = "currencyId"
    const val IS_MAIN_IMAGE = "isMainImage"
    const val BG_URL_KEY = "bg_url"
    const val COLOR_MAIN_BG = "colormainbg"
    const val  COLOR_START_BTN = "startbtnbg"
    const val  COLOR_START_BTN_TXT = "startbtntxt"


}