package com.samaralii.restaurantapp.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.samaralii.restaurantapp.R
import com.samaralii.restaurantapp.data.models.Options

class DetailOptionsAdapter(private val data: List<Options>, private val colorInt: Int, private val symbol: String): RecyclerView.Adapter<DetailOptionsVH>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailOptionsVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_option, parent, false)
        return DetailOptionsVH(v)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: DetailOptionsVH, position: Int) {

        holder.title.setTextColor(colorInt)
        holder.des.setTextColor(colorInt)
        holder.price.setTextColor(colorInt)

        holder.title.text = data[position].title
        holder.des.text = data[position].description
        holder.price.text = "$symbol ${data[position].price}"

    }

}


class DetailOptionsVH(v: View): RecyclerView.ViewHolder(v){
    val title by lazy { v.findViewById<TextView>(R.id.option_title) }
    val des by lazy { v.findViewById<TextView>(R.id.option_description) }
    val price by lazy { v.findViewById<TextView>(R.id.option_price) }
}