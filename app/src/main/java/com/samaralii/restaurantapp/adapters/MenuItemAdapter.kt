package com.samaralii.restaurantapp.adapters

import android.content.Context
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.samaralii.restaurantapp.GlideApp
import com.samaralii.restaurantapp.R
import com.samaralii.restaurantapp.data.models.MenuItem
import com.samaralii.restaurantapp.data.models.SubMenu

class MenuItemAdapter(private val data: List<MenuItem>,
                      private val context: Context,
                      private val onItemClick: (List<SubMenu>, Int) -> Unit
): RecyclerView.Adapter<MenuItemVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuItemVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_category_main, parent, false)
        return MenuItemVH(v)
    }


    override fun onBindViewHolder(holder: MenuItemVH, position: Int) {

        val cat = data[position]
        holder.name.text = cat.name

        cat.filename?.let {
            GlideApp.with(context).load(it).into(holder.image)
        }

        holder.itemView.setOnClickListener {
            onItemClick.invoke(cat.subcategories, position)
        }

    }

    override fun getItemCount() = data.size

}

class MenuItemVH(v: View): RecyclerView.ViewHolder(v){
    val name by lazy { v.findViewById<TextView>(R.id.list_tvName) }
    val image by lazy { v.findViewById<ImageView>(R.id.list_ivImage) }
}