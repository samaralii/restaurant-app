package com.samaralii.restaurantapp.adapters

import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.samaralii.restaurantapp.R
import com.samaralii.restaurantapp.data.models.MenuItem


class NewMenuItemAdapter(private val data: List<MenuItem>, val typeface: Typeface,
                         private val onItemClick: (Int, String) -> Unit
): RecyclerView.Adapter<NewMenuItemVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewMenuItemVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.newmenu_list, parent, false)
        return NewMenuItemVH(v)
    }


    override fun onBindViewHolder(holder: NewMenuItemVH, position: Int) {

        val cat = data[position]

        holder.name.typeface = typeface

        holder.name.text = cat.name

        holder.itemView.setOnClickListener {
            onItemClick.invoke(holder.adapterPosition, cat.name)
        }


    }

    override fun getItemCount() = data.size

}

class NewMenuItemVH(v: View): RecyclerView.ViewHolder(v) {
    val name by lazy { v.findViewById<TextView>(R.id.menulist_tvName) }



}