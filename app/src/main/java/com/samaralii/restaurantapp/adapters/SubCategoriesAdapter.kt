package com.samaralii.restaurantapp.adapters



import android.content.Context
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.samaralii.restaurantapp.GlideApp
import com.samaralii.restaurantapp.R
import com.samaralii.restaurantapp.data.models.SubMenu
import com.samaralii.restaurantapp.data.models.SubMenuItem


class SubCategoriesAdapter(private var data: MutableList<SubMenu> = arrayListOf(),
                        private val titleFont: Typeface,
                        private val desFont: Typeface,
                        private val context: Context,
                        private val onItemClick: (List<SubMenuItem>, String) -> Unit
): RecyclerView.Adapter<SubCategoriesVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubCategoriesVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.submenu_list, parent, false)
        return SubCategoriesVH(v)
    }


    override fun onBindViewHolder(holder: SubCategoriesVH, position: Int) {

        holder.desc.typeface = desFont
        holder.title.typeface = titleFont

        val cat = data[position]
        holder.title.text = cat.name
        holder.desc.text = cat.description

        cat.filename?.let {
            GlideApp.with(context).load(it).into(holder.image)
        }

        holder.itemView.setOnClickListener {
            onItemClick.invoke(cat.items, cat.name)
        }

    }


    fun addData(data: MutableList<SubMenu>) {
        this.data.clear()
        this.data = data
        notifyDataSetChanged()
    }

    override fun getItemCount() = data.size

}

class SubCategoriesVH(v: View): RecyclerView.ViewHolder(v) {
    val title by lazy { v.findViewById<TextView>(R.id.submenu_tvTitle) }
    val desc by lazy { v.findViewById<TextView>(R.id.submenu_tvDesc) }
    val image by lazy { v.findViewById<ImageView>(R.id.submenu_ivImage) }
}