package com.samaralii.restaurantapp.adapters

import android.content.Context
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.samaralii.restaurantapp.GlideApp
import com.samaralii.restaurantapp.R
import com.samaralii.restaurantapp.data.models.SubMenu
import com.samaralii.restaurantapp.data.models.SubMenuItem


class NewSubMenuAdapter(private var data: MutableList<SubMenuItem> = arrayListOf(),
                        private val titleFont: Typeface,
                        private val desFont: Typeface,
                        private val context: Context,
                        private val onItemClick: (String) -> Unit,
                        private val curSymbol: String
): RecyclerView.Adapter<NewSubMenuVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewSubMenuVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.submenu_list, parent, false)
        return NewSubMenuVH(v)
    }


    override fun onBindViewHolder(holder: NewSubMenuVH, position: Int) {

        holder.desc.typeface = desFont
        holder.title.typeface = titleFont
        holder.price.typeface = titleFont

        val cat = data[position]
        holder.title.text = cat.name
        holder.desc.text = cat.description

        cat.filename?.let {
            GlideApp.with(context).load(it).into(holder.image)
        }

        holder.price.text = "$curSymbol ${cat.price}"

        holder.itemView.setOnClickListener {

            cat.filename?.let {url ->
                onItemClick.invoke(url)
            }

        }

    }


    fun addData(data: MutableList<SubMenuItem>) {
        this.data.clear()
        this.data = data
        notifyDataSetChanged()
    }

    override fun getItemCount() = data.size

}

class NewSubMenuVH(v: View): RecyclerView.ViewHolder(v){
    val title by lazy { v.findViewById<TextView>(R.id.submenu_tvTitle) }
    val desc by lazy { v.findViewById<TextView>(R.id.submenu_tvDesc) }
    val price by lazy { v.findViewById<TextView>(R.id.submenu_price) }
    val image by lazy { v.findViewById<ImageView>(R.id.submenu_ivImage) }
}