package com.samaralii.restaurantapp.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.samaralii.restaurantapp.GlideApp
import com.samaralii.restaurantapp.R
import com.samaralii.restaurantapp.data.models.SubMenuItem


class SubMenuItemAdapter(private val data: List<SubMenuItem>,
                         private val context: Context,
                         private val onItemClick: (SubMenuItem, Int) -> Unit,
                         private val curSymbol: String
): RecyclerView.Adapter<SubMenuItemVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubMenuItemVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_category, parent, false)
        return SubMenuItemVH(v)
    }


    override fun onBindViewHolder(holder: SubMenuItemVH, position: Int) {

        val cat = data[position]
        holder.name.text = cat.name

        holder.price.text = "$curSymbol ${cat.price}"


        cat.filename?.let {
            GlideApp.with(context).load(it).into(holder.image)
        }

        holder.itemView.setOnClickListener {
            onItemClick.invoke(cat, position)
        }

    }

    override fun getItemCount() = data.size

}

class SubMenuItemVH(v: View): RecyclerView.ViewHolder(v){
    val name by lazy { v.findViewById<TextView>(R.id.list_tvName) }
    val price by lazy { v.findViewById<TextView>(R.id.list_tvPrice) }
    val priceLayout by lazy { v.findViewById<View>(R.id.list_flPrice) }
    val image by lazy { v.findViewById<ImageView>(R.id.list_ivImage) }
}