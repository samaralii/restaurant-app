package com.samaralii.restaurantapp.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Menu(val items: List<MenuItem>): Parcelable

@Parcelize
data class MenuItem(val name: String, val picture: String?,
                 var filename: String? ,val subcategories: List<SubMenu>): Parcelable

@Parcelize
data class SubMenu(val name: String, val picture: String?,
                   var filename: String?, val items: List<SubMenuItem>, val description: String): Parcelable

@Parcelize
data class SubMenuItem(val id: String, val category: String, val sub_category: String,
                       val name: String, val picture: String?, val ismenu: String,
                       var filename: String?, val description: String, val price: String,
                       val preparation_time: String, val calories: String, val options: List<Options>?): Parcelable


@Parcelize
data class Options(val id: String, val item_id: String,
                   var title: String, val description: String, val price: String): Parcelable