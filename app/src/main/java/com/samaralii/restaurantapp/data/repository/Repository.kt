package com.samaralii.restaurantapp.data.repository

import android.content.SharedPreferences
import android.util.Log
import com.samaralii.restaurantapp.AppConst
import com.samaralii.restaurantapp.Provider
import com.samaralii.restaurantapp.data.models.MenuItem
import com.google.gson.reflect.TypeToken
import com.samaralii.restaurantapp.data.models.SubMenu
import com.samaralii.restaurantapp.data.models.SubMenuItem


class Repository(private val sp: SharedPreferences) {


    fun addBaseUrl(url: String) {

        val edit = sp.edit()
        edit.putString(AppConst.BASE_URL_KEY, url)
        edit.apply()

    }

    fun getBaseUrl() = sp.getString(AppConst.BASE_URL_KEY, null)


    fun saveData(data: List<MenuItem>) {
        val edit = sp.edit()

        val gson = Provider.provideGson()
        val json = gson.toJson(data)

        Log.d("JSON", json)

        edit.putString(AppConst.DATA, json)
        edit.apply()
    }


    fun getData(): List<MenuItem>? {

        val json = sp.getString(AppConst.DATA, null) ?: return null

        val gson = Provider.provideGson()

        val listType = object : TypeToken<List<MenuItem>>() {}.type

        return gson.fromJson(json, listType)

    }




    fun saveValue(key: String, value: Any) {
        val edit = sp.edit()

        when(value) {
            is String -> edit.putString(key, value)
            is Int -> edit.putInt(key, value)
            is Boolean -> edit.putBoolean(key, value)
        }

        edit.apply()

    }


    fun saveSubMenuItems(data: List<SubMenuItem>): String {
        val gson = Provider.provideGson()
        val json = gson.toJson(data)
        Log.d("JSON", json)
        return json
    }
    fun getSubMenuItems(json: String): List<SubMenuItem> {
        val gson = Provider.provideGson()
        val listType = object : TypeToken<List<SubMenuItem>>() {}.type
        return gson.fromJson(json, listType)
    }

    fun saveSubMenu(data: List<SubMenu>): String {
        val gson = Provider.provideGson()
        val json = gson.toJson(data)
        Log.d("JSON", json)
        return json
    }
    fun getSubMenu(json: String): List<SubMenu> {
        val gson = Provider.provideGson()
        val listType = object : TypeToken<List<SubMenu>>() {}.type
        return gson.fromJson(json, listType)
    }

}