package com.samaralii.restaurantapp.data.models

data class Currency(val id: Int, val name: String, val symbol: String, var isSelected: Boolean)