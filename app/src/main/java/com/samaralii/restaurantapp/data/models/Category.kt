package com.samaralii.restaurantapp.data.models

import android.os.Parcelable
import android.support.annotation.DrawableRes
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Category(val name: String, @DrawableRes val imageId: Int, var isSelected: Boolean = false): Parcelable