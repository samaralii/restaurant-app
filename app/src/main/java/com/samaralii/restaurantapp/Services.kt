package com.samaralii.restaurantapp

import com.google.gson.JsonElement
import com.samaralii.restaurantapp.data.models.Menu
import com.samaralii.restaurantapp.data.models.MenuItem
import kotlinx.coroutines.Deferred
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.GET
import retrofit2.http.Url

interface Services {

    @GET("api/all")
    fun login(@Field("email") email: String, @Field("password") password: String):
            Deferred<Response<JsonElement>>

    @GET
    fun getAllData(@Url url: String): Deferred<Response<Menu>>

    @GET
    fun downloadImage(@Url url: String): Deferred<ResponseBody>

}